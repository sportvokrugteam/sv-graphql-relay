/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow
 */

import { describe, it } from 'mocha';
import { expect } from 'chai';

import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  graphql
} from 'graphql';

import {
  nodeDefinitions
} from '../node';

const userData = {
  '1': {
    _id: 1,
    name: 'John Doe'
  },
  '2': {
    _id: 2,
    name: 'Jane Smith'
  },
};

const { nodeField, nodeInterface } = nodeDefinitions(
  _id => {
    return userData[_id];
  },
  () => {
    return userType;
  }
);

const userType = new GraphQLObjectType({
  name: 'User',
  interfaces: [ nodeInterface ],
  fields: () => ({
    _id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    name: {
      type: GraphQLString,
    },
  })
});

const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField
  })
});

const schema = new GraphQLSchema({
  query: queryType,
  types: [ userType ]
});

describe('Node interface and fields with async object fetcher', () => {
  it('gets the correct ID for users', async () => {
    const query = `{
      node(_id: "1") {
        _id
      }
    }`;

    return expect(await graphql(schema, query)).to.deep.equal({
      data: {
        node: {
          _id: '1',
        }
      }
    });
  });

  it('gets the correct name for users', async () => {
    const query = `{
      node(_id: "1") {
        _id
        ... on User {
          name
        }
      }
    }`;

    return expect(await graphql(schema, query)).to.deep.equal({
      data: {
        node: {
          _id: '1',
          name: 'John Doe',
        }
      }
    });
  });
});
