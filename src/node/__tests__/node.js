/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow
 */

import { describe, it } from 'mocha';
import { expect } from 'chai';

import {
  GraphQLID,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  graphql
} from 'graphql';

import {
  nodeDefinitions
} from '../node';

const userData = {
  '1': {
    _id: 1,
    name: 'John Doe'
  },
  '2': {
    _id: 2,
    name: 'Jane Smith'
  },
};

const photoData = {
  '3': {
    _id: 3,
    width: 300
  },
  '4': {
    _id: 4,
    width: 400
  },
};

const { nodeField, nodesField, nodeInterface } = nodeDefinitions(
  (_id, context, info) => {
    expect(info.schema).to.equal(schema);
    if (userData[_id]) {
      return userData[_id];
    }
    if (photoData[_id]) {
      return photoData[_id];
    }
  },
  obj => {
    if (userData[obj._id]) {
      return userType;
    }
    if (photoData[obj._id]) {
      return photoType;
    }
  }
);

const userType = new GraphQLObjectType({
  name: 'User',
  interfaces: [ nodeInterface ],
  fields: () => ({
    _id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    name: {
      type: GraphQLString,
    },
  })
});

const photoType = new GraphQLObjectType({
  name: 'Photo',
  interfaces: [ nodeInterface ],
  fields: () => ({
    _id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    width: {
      type: GraphQLInt,
    },
  })
});

const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField,
    nodes: nodesField
  })
});

const schema = new GraphQLSchema({
  query: queryType,
  types: [ userType, photoType ]
});

describe('Node interface and fields', () => {
  describe('refetchability', () => {
    it('gets the correct ID for users', async () => {
      const query = `{
        node(_id: "1") {
          _id
        }
      }`;

      expect(await graphql(schema, query)).to.deep.equal({
        data: {
          node: {
            _id: '1',
          }
        }
      });
    });

    it('gets the correct IDs for users', async () => {
      const query = `{
        nodes(ids: ["1", "2"]) {
         _id
        }
      }`;

      expect(await graphql(schema, query)).to.deep.equal({
        data: {
          nodes: [
            {
              _id: '1'
            },
            {
              _id: '2'
            }
          ]
        }
      });
    });

    it('gets the correct ID for photos', async () => {
      const query = `{
        node(_id: "4") {
          _id
        }
      }`;

      expect(await graphql(schema, query)).to.deep.equal({
        data: {
          node: {
            _id: '4',
          }
        }
      });
    });

    it('gets the correct IDs for photos', async () => {
      const query = `{
        nodes(ids: ["3", "4"]) {
          _id
        }
      }`;

      expect(await graphql(schema, query)).to.deep.equal({
        data: {
          nodes: [
            {
              _id: '3'
            },
            {
              _id: '4'
            }
          ]
        }
      });
    });

    it('gets the correct IDs for multiple types', async () => {
      const query = `{
        nodes(ids: ["1", "3"]) {
          _id
        }
      }`;

      expect(await graphql(schema, query)).to.deep.equal({
        data: {
          nodes: [
            {
              _id: '1'
            },
            {
              _id: '3'
            }
          ]
        }
      });
    });

    it('gets the correct name for users', async () => {
      const query = `{
        node(_id: "1") {
          _id
          ... on User {
            name
          }
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          node: {
            _id: '1',
            name: 'John Doe',
          }
        }
      });
    });

    it('gets the correct width for photos', async () => {
      const query = `{
        node(_id: "4") {
          _id
          ... on Photo {
            width
          }
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          node: {
            _id: '4',
            width: 400,
          }
        }
      });
    });

    it('gets the correct type name for users', async () => {
      const query = `{
        node(_id: "1") {
          _id
          __typename
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          node: {
            _id: '1',
            __typename: 'User',
          }
        }
      });
    });

    it('gets the correct type name for photos', async () => {
      const query = `{
        node(_id: "4") {
          _id
          __typename
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          node: {
            _id: '4',
            __typename: 'Photo',
          }
        }
      });
    });

    it('ignores photo fragments on user', async () => {
      const query = `{
        node(_id: "1") {
          _id
          ... on Photo {
            width
          }
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          node: {
            _id: '1',
          }
        }
      });
    });

    it('returns null for bad IDs', async () => {
      const query = `{
        node(_id: "5") {
          _id
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          node: null
        }
      });
    });

    it('returns nulls for bad IDs', async () => {
      const query = `{
        nodes(ids: ["3", "5"]) {
          _id
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          nodes: [
            {
              _id: '3'
            },
            null
          ]
        }
      });
    });
  });

  describe('introspection', () => {
    it('has correct node interface', async () => {
      const query = `{
        __type(name: "Node") {
          name
          kind
          fields {
            name
            type {
              kind
              ofType {
                name
                kind
              }
            }
          }
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          __type: {
            name: 'Node',
            kind: 'INTERFACE',
            fields: [
              {
                name: '_id',
                type: {
                  kind: 'NON_NULL',
                  ofType: {
                    name: 'ID',
                    kind: 'SCALAR'
                  }
                }
              }
            ]
          }
        }
      });
    });

    it('has correct node and nodes root fields', async () => {
      const query = `{
        __schema {
          queryType {
            fields {
              name
              type {
                name
                kind
              }
              args {
                name
                type {
                  kind
                  ofType {
                    name
                    kind
                  }
                }
              }
            }
          }
        }
      }`;

      return expect(await graphql(schema, query)).to.deep.equal({
        data: {
          __schema: {
            queryType: {
              fields: [
                {
                  name: 'node',
                  type: {
                    name: 'Node',
                    kind: 'INTERFACE'
                  },
                  args: [
                    {
                      name: '_id',
                      type: {
                        kind: 'NON_NULL',
                        ofType: {
                          name: 'ID',
                          kind: 'SCALAR'
                        }
                      }
                    }
                  ]
                },
                {
                  name: 'nodes',
                  type: {
                    name: null,
                    kind: 'NON_NULL'
                  },
                  args: [
                    {
                      name: 'ids',
                      type: {
                        kind: 'NON_NULL',
                        ofType: {
                          name: null,
                          kind: 'LIST'
                        }
                      }
                    }
                  ]
                }
              ]
            }
          }
        }
      });
    });
  });
});
