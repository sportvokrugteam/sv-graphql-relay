/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow
 */

import { expect } from 'chai';
import { describe, it } from 'mocha';
import { StarWarsSchema } from './starWarsSchema.js';
import { graphql } from 'graphql';

// 80+ char lines are useful in describe/it, so ignore in this file.
/* eslint-disable max-len */

describe('Star Wars object identification', () => {
  it('fetches the ID and name of the rebels', async () => {
    const query = `
      query RebelsQuery {
        rebels {
          _id
          name
        }
      }
    `;
    const expected = {
      rebels: {
        _id: 'RmFjdGlvbjox',
        name: 'Alliance to Restore the Republic'
      }
    };
    const result = await graphql(StarWarsSchema, query);
    expect(result).to.deep.equal({ data: expected });
  });

  it('refetches the rebels', async () => {
    const query = `
      query RebelsRefetchQuery {
        node(_id: "RmFjdGlvbjox") {
          _id
          ... on Faction {
            name
          }
        }
      }
    `;
    const expected = {
      node: {
        _id: 'RmFjdGlvbjox',
        name: 'Alliance to Restore the Republic'
      }
    };
    const result = await graphql(StarWarsSchema, query);
    expect(result).to.deep.equal({ data: expected });
  });

  it('fetches the ID and name of the empire', async () => {
    const query = `
      query EmpireQuery {
        empire {
          _id
          name
        }
      }
    `;
    const expected = {
      empire: {
        _id: 'RmFjdGlvbjoy',
        name: 'Galactic Empire'
      }
    };
    const result = await graphql(StarWarsSchema, query);
    expect(result).to.deep.equal({ data: expected });
  });

  it('refetches the empire', async () => {
    const query = `
      query EmpireRefetchQuery {
        node(_id: "RmFjdGlvbjoy") {
          _id
          ... on Faction {
            name
          }
        }
      }
    `;
    const expected = {
      node: {
        _id: 'RmFjdGlvbjoy',
        name: 'Galactic Empire'
      }
    };
    const result = await graphql(StarWarsSchema, query);
    expect(result).to.deep.equal({ data: expected });
  });

  it('refetches the X-Wing', async () => {
    const query = `
      query XWingRefetchQuery {
        node(_id: "U2hpcDox") {
          _id
          ... on Ship {
            name
          }
        }
      }
    `;
    const expected = {
      node: {
        _id: 'U2hpcDox',
        name: 'X-Wing'
      }
    };
    const result = await graphql(StarWarsSchema, query);
    expect(result).to.deep.equal({ data: expected });
  });
});
